require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|words| sub_string?(words, substring)}
end

def sub_string?(string1, string2)
  string2.each_char.all? {|char| string1.include?(char)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = Hash.new(0)

  string.each_char do |char|
    letters[char] += 1 unless char == " "
  end

  letters.keys.select { |key| letters[key] > 1 }
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(" ")

  words.sort_by { |word| word.length }[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z")
  alphabet.reject { |letter| string.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |years| not_repeat_year?(years)}
end

def not_repeat_year?(year)
  year_array = year.to_s.split("")
  year_array == year_array.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select { |wonders| no_repeats?(wonders, songs) }.uniq
end

def no_repeats?(song_name, songs)
  songs.each_index do |idx|
    if songs[idx] == songs[idx + 1] && songs [idx] == song_name
      return false
    end
  end

  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  punctuation = ".,:;?!'-"
  c_words = c_string(string).delete(punctuation).split(" ")
  c_words.sort_by { |word| c_distance(word) }[0]
end

def c_distance(word)
  word.reverse.index("c")
end

def c_string(string)
  string.split(" ").select { |words| words.include?("c") }.join(" ")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  index_ranges = []
  starting_range = nil

  arr.each_with_index do |num, idx|

    if num == arr[idx + 1]
      starting_range = idx unless starting_range
    elsif starting_range
      index_ranges << [starting_range, idx]
      starting_range = nil
    end
  end

  index_ranges
end
